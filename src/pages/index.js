import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import {Image} from 'react-bootstrap'
import me from "../images/me.jpg"
import Seo from "../components/seo"

const IndexPage = () => (
  <Layout>
    <Seo title="Home" />
    
    <Image style={{ maxWidth: `300px`, marginBottom: `1.45rem` }}className="rounded" src={me} />

    <h3>Hi, I'm Phil. I'm a Software Craftsman in Milwaukee, Wisconsin, USA.</h3>
    <p>Currently, I'm at <Link to="https://www.layeronemedia.com">Layer One</Link> as Senior Director of Software Delivery. At Layer One, I am spending a lot of time architecting our development teams and client interactions for successful business outcomes.</p>
    <p>The work I'm doing is constantly evolving, but I like to keep these things present in my professional life:</p>
    <ul>
      <li>Creating interactions between individuals on projects</li>
      <li>Writing scalable and testable code</li>
      <li>Learning new technologies</li>
      <li>Clean architecture</li>
    </ul>
    <p>I have a technical background in delivering for the web. I have experience in .NET, Java, and many front end frameworks such as React, Angular, and Gatsby. I have over a decade of experience delivering with these technologies in various verticals, including marketing tech, insurance, and material handling.</p>
    <p>I like to share the knowledge I have with others in a written form. Please check out what I have to say on my <Link to="/blog">blog</Link>. Additionally, I contribute to <Link to="https://deglutino.com">Deglutino</Link>, a blog focused on software development approach and craft.</p>
    <p>I also speak publicly from time to time. You can see a catalog of my previous meetup talks on my <Link to="/speaking">speaking page</Link>.</p>
    <p>Want to chat? Shoot me an email: phil dot busch at gmail dot com. Or, find me on <Link to="https://www.linkedin.com/in/philbusch/">LinkedIn</Link>.</p>

  </Layout>
)

export default IndexPage
