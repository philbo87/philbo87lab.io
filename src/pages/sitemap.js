import React from "react"

import Layout from "../components/layout"
import Seo from "../components/seo"
import Sitemap from "../components/SitemapComponent"
const SitemapPage = () => (
  <Layout>
    <Seo title="Sitemap" />
    <h1>Sitemap</h1>
    <Sitemap/>
  </Layout>
)

export default SitemapPage
