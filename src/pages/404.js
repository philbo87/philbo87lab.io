import React from "react"

import Layout from "../components/layout"
import Seo from "../components/seo"

const NotFoundPage = () => (
  <Layout>
    <Seo title="404: Not found" />
    <h1>404 Not Found</h1>
    <p>Sorry, but this is not a page on my website.</p>
  </Layout>
)

export default NotFoundPage
