import * as React from 'react'
import { graphql } from 'gatsby'
import { CardColumns } from 'react-bootstrap'
import BlogListItem from '../components/BlogListItem'
import Layout from '../components/layout'
import Seo from '../components/seo'

const BlogPage = ({ data }) => {
    return (
        <Layout>
            <Seo title="Blog" description="Thoughts written by Phil Busch" />
            <h2>Blog</h2>
            <p class="lead">
                Where I am writing my thoughts. Recent posts shown below.
            </p>
            <CardColumns>
                {
                    data.allMdx.edges.map((edge, idx) => 
                    {
                      if(edge.node.frontmatter.isPublished)
                        return <BlogListItem idx={idx} blogData={edge.node} />
                      else
                        return undefined;
                    })
                }
            </CardColumns>
        </Layout>
    )
}

export const query = graphql`
query blogPageQuery {
  site {
    siteMetadata {
      title
      description
    }
  }
  allMdx(sort: {order: DESC, fields: [frontmatter___date]} filter:{ frontmatter: { type: { eq: "blog"}}}) {
    edges {
      node {
        id
        excerpt(pruneLength: 250)
        slug
        frontmatter {
          date(formatString: "YYYY-MM-DD")
          title
          isPublished
          type
        }
      }
    }
  }
}
`
export default BlogPage