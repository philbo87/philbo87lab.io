import * as React from 'react'
import { graphql } from 'gatsby'
import EventsList from '../components/EventsList'
import Layout from '../components/layout'
import Seo from '../components/seo'

const SpeakingPage = ({ data }) => {
    return (
        <Layout>
            <Seo title="Speaking Engagements" description="Occasions Phil Busch has spoken publicly" />
            <h2>Speaking Engagements</h2>
            <p class="lead">
                From time to time, I like to speak at meetups and events when I have knowledge to share with the community. Below is a list of times I've spoken publicly.
            </p>
            <EventsList />
        </Layout>
    )
}

export const query = graphql`
query speakingPageQuery {
  site {
    siteMetadata {
      title
      description
    }
  }
}
`
export default SpeakingPage