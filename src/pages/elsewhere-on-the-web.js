import * as React from 'react'
import Layout from '../components/layout'
import Seo from '../components/seo'
import ExternalContentList from '../components/ExternalContentList/externalContentList';

const ElsewhereOnTheWeb = ({ data }) => {
  return (
<Layout>
            <Seo title="Elsewhere on the Web" description="Other places Phil Busch has content published online" />
            <h2>Elsewhere on the Web</h2>
            <p class="lead">
                Here's where you can find other content of mine on the web.
            </p>
            <ExternalContentList />
        </Layout>
  );
};

export default ElsewhereOnTheWeb;