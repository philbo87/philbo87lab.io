import {Navbar, Nav } from 'react-bootstrap'
import PropTypes from "prop-types"
import React from "react"

const Header = ({ siteTitle, tagline }) => (
  <header
    style={{
      //background: `rebeccapurple`,
      marginBottom: `1.45rem`,
    }}
  >
    <div
      style={{
        margin: `0 auto`,
        maxWidth: 960,
        padding: `1.45rem 1.0875rem`,
      }}
    >
      <Navbar bg="" expand="lg">
      <Navbar.Brand style={{ fontWeight: 500 }}href="/">{siteTitle}</Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="mr-auto">
          <Nav.Link href="/blog">Blog</Nav.Link>
          <Nav.Link href="/speaking">Speaking</Nav.Link>
          <Nav.Link href="/elsewhere-on-the-web">Elsewhere on the Web</Nav.Link>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
    </div>

  </header>
)

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header
