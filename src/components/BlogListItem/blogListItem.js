import * as React from "react"
import { Card, Col } from 'react-bootstrap'
import { Link } from 'gatsby'

const BlogListItem = ({ blogData }) => {
    
    var dateArray = blogData.frontmatter.date.split('-');

    return (
        <Col className="py-2">
            <Link to={`/blog/${dateArray[0]}/${dateArray[1]}/${blogData.slug}`} style={{ textDecoration: `none`}}>
                <Card bg="light" border="secondary" text="dark">
                    <Card.Header className={`d-flex align-items-center`} style={{ "min-height": "20%" }} >{blogData.frontmatter.title}</Card.Header>
                    <Card.Body>
                        <Card.Text>{blogData.excerpt}</Card.Text>
                    </Card.Body>
                </Card>
            </Link>
        </Col>
)}

export default BlogListItem