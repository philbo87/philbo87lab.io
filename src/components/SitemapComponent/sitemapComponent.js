import * as React from "react"
import { Container } from 'react-bootstrap'
import { Link, graphql, useStaticQuery } from 'gatsby'

const SitemapComponent = () => {
  const data = useStaticQuery(graphql`
  query {
    allSitePage {
      edges {
        node {
          path
          pageContext 
        }
      }
    }
  }
  `)

  function formatPageTitle(pagePath) {
    return pagePath.replace('/','').replace('/','').replace(".html","").charAt(0).toUpperCase() + pagePath.replace('/','').replace('/','').replace(".html","").slice(1);
  }

  return (
    <>
      {<Container>
        {
          data.allSitePage.edges.map((edge) => {
            const linkText = edge.node.pageContext.post !== undefined ? edge.node.pageContext.post.frontmatter.title : formatPageTitle(edge.node.path)
            return (
              <>
                <div>
                  {
                    edge.node.path && edge.node.pageContext &&
                    <Link to={edge.node.path}>
                      {
                        linkText
                      }
                    </Link>
                  }
                </div>
              </>
            );

          })
        }
      </Container>}
    </>
  )
}

export default SitemapComponent