import * as React from "react"
import { Card, Col, Container, Row } from 'react-bootstrap'
import { Link, graphql, useStaticQuery } from 'gatsby'

const EventsList = () => {
  const data = useStaticQuery(graphql`
    query allEvents {
      allMdx(sort: {order: DESC, fields: [frontmatter___date]} filter:{ frontmatter: { type: { eq: "event"}}}) {
        edges {
          node {
            id
            excerpt(pruneLength: 250)
            slug
            frontmatter {
              date(formatString: "dddd, MMMM DD, YYYY")
              presentationName
              eventHost
              location
              repoUrl
              slidesUrl
              externalUrl
              linkedInUrl
              externalPosts
              {
                name
                url
              }
            }
          }
        }
      }
    }
    
  `)
  return (
    <>
      <Container>
        {
          data.allMdx.edges.map((edge) => {
            return (
              <>
                <Row className="pt-2 pb-2">
                  <Col>
                    <Card>
                      <Card.Header as="h5">{edge.node.frontmatter.presentationName}</Card.Header>
                      <Card.Body>
                        <Card.Text>
                          <Container>
                            <Row>
                              <Col md={2} className="fw-bold">Date:</Col> <Col>{edge.node.frontmatter.date} </Col>
                            </Row>
                            <Row>
                              <Col md={2} className="fw-bold">Event Host:</Col> <Col>{edge.node.frontmatter.eventHost}</Col>
                            </Row>
                            <Row>
                              <Col md={2} className="fw-bold">Location:</Col><Col> {edge.node.frontmatter.location}</Col>
                            </Row>
                            <Row className="pt-1">
                              {edge.node.frontmatter.repoUrl &&
                                <Col md={3}>
                                  <Link to={edge.node.frontmatter.repoUrl} target="_blank" >Git Repository <small>[↗]</small></Link>
                                </Col>
                              }
                              {edge.node.frontmatter.slidesUrl &&
                                <Col md={2}>
                                  <Link to={edge.node.frontmatter.slidesUrl} target="_blank">Slides <small>[↗]</small></Link>
                                </Col>
                              }
                              {edge.node.frontmatter.externalUrl &&
                                <Col md={3}>
                                  <Link to={edge.node.frontmatter.externalUrl} target="_blank">View on Meetup.com <small>[↗]</small></Link>
                                </Col>
                              }
                              {edge.node.frontmatter.linkedInUrl &&
                                <Col md={3}>
                                  <Link to={edge.node.frontmatter.linkedInUrl} target="_blank">View on LinkedIn <small>[↗]</small></Link>
                                </Col>
                              }
                              {
                                edge.node.frontmatter.externalPosts &&
                                  edge.node.frontmatter.externalPosts.map((post) => (
                                    <>
                                      <Col md={3}>
                                        <Link to={post.url} target="_blank">View: {post.name} <small>[↗]</small></Link>
                                      </Col>
                                    </>
                                  ))
                                
                              }
                            </Row>
                          </Container>
                        </Card.Text>
                      </Card.Body>
                    </Card>
                  </Col>
                </Row>
              </>
            );

          })
        }
      </Container>
    </>
  )
}

export default EventsList