import * as React from "react"
import { Card, Col, Container, Row } from 'react-bootstrap'
import { Link, graphql, useStaticQuery } from 'gatsby'

const ExternalContentList = () => {
  const data = useStaticQuery(graphql`
    query allExternalContent {
      allMdx(
        sort: {order: DESC, fields: [frontmatter___date]}
        filter: {frontmatter: {type: {eq: "externalContent"}}}
      ) {
        edges {
          node {
            id
            slug
            frontmatter {
              date(formatString: "dddd, MMMM DD, YYYY")
              title
              url
              location
            }
          }
        }
      }
    }

    
  `)
  return (
    <>
      <Container>
        {
          data.allMdx.edges.map((edge) => {
            return (
              <>
                <Row className="pt-2 pb-2">
                  <Col>
                    <Card>
                      <Card.Header as="h5">{edge.node.frontmatter.title}</Card.Header>
                      <Card.Body>
                        <Card.Text>
                          <Container>
                            <Row>
                              <Col md={2} className="fw-bold">Date:</Col> <Col>{edge.node.frontmatter.date} </Col>
                            </Row>
                            <Row>
                              <Col md={2} className="fw-bold">Location:</Col><Col> {edge.node.frontmatter.location}</Col>
                            </Row>
                            <Row className="pt-1">
                              {edge.node.frontmatter.url &&
                                  <Link to={edge.node.frontmatter.url} target="_blank">Click to view on {edge.node.frontmatter.location} <small>[↗]</small></Link>
                              }
                            </Row>
                          </Container>
                        </Card.Text>
                      </Card.Body>
                    </Card>
                  </Col>
                </Row>
              </>
            );

          })
        }
      </Container>
    </>
  )
}

export default ExternalContentList