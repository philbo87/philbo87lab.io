import * as React from 'react'
import Layout from '../components/layout'
import Seo from '../components/seo'
import { Link } from "gatsby"
import { Col, Row } from "react-bootstrap"
import { MDXRenderer } from "gatsby-plugin-mdx"
import * as containerStyles from "./blog-detail.module.css"

export default function BlogDetailTemplate({ pageContext }) {

  const { post } = pageContext;
  return (
    <Layout>
      <Seo title={post.frontmatter.title} description={post.excerpt} image={post.frontmatter.imagePath} />
      <div>
        <h2 className={`display-5 fs-1 fw-bold`}>{post.frontmatter.title}</h2>
        <div className={`px-2`}>
          <Row className="align-items-center">
            <Col xs={2} lg="auto">
              <strong>Date:</strong>
            </Col>
            <Col xs={10} lg="auto">
              {post.frontmatter.date}
            </Col>
          </Row>
          <div className={`${containerStyles.body} py-4`}>
            <MDXRenderer>{post.body}</MDXRenderer>
          </div>
          
        </div>
        <div className="mx-auto mb-4">        
          <Link to="/blog" className={`btn btn-primary`}>Read More Blog Posts</Link>
        </div>
      </div>
    </Layout>
  )
}