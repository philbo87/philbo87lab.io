const path = require(`path`)

exports.onCreateWebpackConfig = ({ actions }) => {
  actions.setWebpackConfig({
    devtool: 'eval-source-map',
  })
}

exports.createPages = async ({ graphql, actions }) => {
    const {createRedirect, createPage} = actions //actions is collection of many actions - https://www.gatsbyjs.org/docs/actions
    createRedirect({ fromPath: '/sitemap.xml', toPath: '/sitemap/sitemap-index.xml', isPermanent: true });

    const blogPostTemplate = path.resolve(`src/templates/blog-detail-template.js`)

    const result = await graphql(`
    {
      allMdx(
        sort: { order: DESC, fields: [frontmatter___date] }
        limit: 1000
        filter:{ frontmatter: { type: { eq: "blog"}}}
      ) {
        edges {
          node {
            id
            body
            excerpt
            slug
            frontmatter {
              date(formatString: "YYYY-MM-DD")
              author
              title
              imagePath
              isPublished
            }
          }
        }
      }
    }
  `)

  // Handle errors
  if (result.errors) {
    reporter.panicOnBuild(`Error while running GraphQL query.`)
    return
  }

  result.data.allMdx.edges.forEach(edge => {
    var dateArray = edge.node.frontmatter.date.split('-');
    
    if(edge.node.frontmatter.isPublished){
      createPage({
        path: `/blog/${dateArray[0]}/${dateArray[1]}/${edge.node.slug}`,
        component: blogPostTemplate,
        context: {
          post: edge.node
        }, // additional data can be passed via context
      })
    }
    
  })
}
