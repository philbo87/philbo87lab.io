---
type: blog
date: 2022-05-22
title: Solve Business Problems with Tracer Bullets
author: Phil Busch
imagePath: /blog-images/arrow-target.jpg
isPublished: true
---
import ContainerizedImage from "../src/components/ContainerizedImage"
import diagram1 from "../src/images/blog-images/TracerBulletsDiagram1.png"
import diagram2 from "../src/images/blog-images/TracerBulletsDiagram2.png"
import diagram3 from "../src/images/blog-images/TracerBulletsDiagram3.png"
import diagram4 from "../src/images/blog-images/TracerBulletsDiagram4.png"

I've recently found myself discussing a technique at work called the Tracer Bullet Technique. This technique is something I first learned in a book called _[The Pragmatic Programmer](https://www.amazon.com/Pragmatic-Programmer-Journeyman-Master/dp/020161622X)_ by Andrew Hunt and David Thomas. 

The tracer bullet technique enables learning about a system by making small, interconnected, iterative changes to that system. _The Pragmatic Programmer_ describes it best as "code that glows in the dark." Code written using the tracer bullet technique reveals the all the locations you need to change to solve a problem.

### How to use the Tracer Bullet Technique
Let's say you onboard to a new team, and the first user story you pick up is to build a dashboard that allows a user to view and edit their account information on a website. 

How do you discover how to implement this change? Ideally, you have great documentation and a team member to pair with, but potentially you don't. In that case, you've got to start writing code somewhere. 

#### Find the place to start
There is no magic formula for finding the first place to affect change. If you have prior experience with the technology you're using, or you believe you understand the solution design by reading the code, you can lean on that to identify a place for change. 

If you're unsure where to start, take an educated guess of what code files you will be editing. Set some breakpoints, and execute your application. Breakpoints that you hit are places you can begin coding safely. If you can't run the application, get that working first. 

#### Make a small change
The next step is to change some code. Potentially the previous step led you to discover an API controller in use on one of the pages in the application. Change the response body to include a hardcoded property set to a dummy string ("test"), and verify you see that property when you execute the code.

By making this small change, you're building your knowledge of the software's architecture. Your mental model now looks like this:

<ContainerizedImage src={diagram1} maxWidth={`290px`} />

At this point, you might want to flush out and finish that API response body. **Don't.** Instead, it is time to identify a tangential change to make.

#### Make a Tangential Change

Now you'll want to make a change to a piece of code that interacts with your first change. 

In our API/UI example, the second change you might make is to learn how to utilize the hardcoded test property from the API response and display it in the markup on the page. This change would confirm that you know how to get API response data onto the UI.

As you make this change, take a look around and discover other potential areas of impact. Once you have the word "test" showing up in the markup, you'll may see that the markup is unstyled. Look around for some style sheets, SASS, or LESS files. Your mental model might look like this now:

<ContainerizedImage src={diagram2} maxWidth={`392px`} />

You may be thinking it is time to style that markup. Fight that urge. It's time to make a change in a different spot.

#### Make a Tangential Change in a different piece of the code near your first change
You now want to repeat the same process in a different direction in the stack. Our previous changes went "up" to the UI. Now, you'll want to go "down" towards the data. Perhaps the next change moves your hardcoded test property to be populated by a piece of code in a data access layer you discover, and mapped to an API response body.

Now this might be the solution you see:

<ContainerizedImage src={diagram3}  />

#### Get to the Edges
At this point the architecture has become clear. Make small changes to round out your data to come from your data store, and style your dummy text. This can get you to a full understanding of the architecture:

<ContainerizedImage src={diagram4}/>

#### Build the smallest feature possible
Now that you can affect change everywhere that matters, you can make a pivot to delivering value. Pick the smallest requirement you can identify from your user story and implement it. That may be as simple as displaying the user's first and last name on the profile screen you are building. 

As you now understand the architecture, implementing this will fall into place. 

#### Get feedback
Once you have the feature working, you need to find out if the other requirements on your user story are still valuable at this time. Show a decision-maker what you have created. Ask them questions like these:
- What do you think of this change?
- Can we go live with just this functionality as-is?
- Do we still want the other requirements on this user story as originally planned?

#### Iterate
You'll likely be asked to tweak what you've created or add more functionality. Now that you have knowledge of how to change the whole solution, that won't be a problem. It should be easy for you to start making tweaks, get feedback, and eventually go live. 