---
type: blog
date: 2021-10-16
title: Don't Skip the Hard Parts
author: Phil Busch
imagePath: /blog-images/figureitout.jpg
isPublished: true
---
import ContainerizedImage from "../src/components/ContainerizedImage"

How do you grow in your career? You have likley asked yourself this question. You probably want to get to the next rank or job title. You may want to obtain a higher salary. Usually obtaining this requires learning some new skills and taking on more responsibilities. I've asked this question myself a few times in my career.

You have options with regards to how you go about growing. You can ask your employer for a job description for the next rank up. You can shop around on the job market to see if maybe another company is interested in paying a little more for the skills you already have. These are valid options, and they might get you some of what you're looking for. But, how do you ensure that you _actually grow_?

You grow by confronting the hard parts of the job - head on.

Often when we're looking around on the job market, we're looking for something a little cushier. We want to avoid that crappy waterfall process/annoying technology/problem du jour that is bugging us at the current gig. Maybe find a place with free lunches or standing desks. It's good to look for those things in a job. But in order to truly grow, we need to have hard problems to solve when we get there.

If we get to a place that already has perfect tech, no problems delivering, great communication between stakeholders, and amazing sales, there probably won't be much to stretch you when you get there. There won't be any hard parts to the job. Clock in, write some code, clock out. Where is that going to get you in the next five years when you're looking for something new again? What kind of story will you have to tell? _Everyone_ who is a software developer for more than a week is going to deliver some code. How will you stand out?

Sometimes the hard parts come to you, and sometimes you have to go find them. Before I knew I needed to find the hard parts, they seemed to come to me. When I was a junior engineer, the hard parts were a daily part of the job. Getting the code to work? Hard part. Understanding the task at hand? Hard part. Deploying to production by _myself?!?_ Really hard part.

Over time, many of the things that were hard have become easier. With practice, I've been able to build the muscles of delivering working software. I've found that as I've gotten better delivering software, I've had to look around for new hard problems. At first, switching to a new tech stack was a hard problem to tackle. Then I switched technologies a few times and that stopped being so difficult. Code problems are code problems, dependencies are important to manage, how to deploy is something to understand early. All of those problems started to look the same, no matter what the language or stack.

You may have reached this point yourself. If you're at this stage, you have to be extra  intentional about growing yourself. Many developers are shielded from the bigger problems to solve within a company. You may be viewed as someone who just codes what is needed for the business. In my experience, to grow beyond that point, you have to find the unsolved problems in your company that you can fix. 

You can go about this many different ways. It may involve looking outside your current team, department, or even employer. You need to start talking to others and listening to their problems. Book lunches or catch a coffee with people you wouldn't normally overlap with. Catch a beer with a sales guy. Talk to the Product Manager you see once a quarter. There is not a formula for finding these problems - you just have to take initiative to look around for them. Once you find these problems, you may be doing them alongside your day job. An hour here, a Saturday morning there. Over time, if you deliver value, you'll find a way to make them part of your day to day.

If you're not finding these problems in your current employer, it is probably time to look around. But don't just look for that cushy office or pretty tech - look for the interesting problems. For senior engineers, they are often the key to real personal growth.