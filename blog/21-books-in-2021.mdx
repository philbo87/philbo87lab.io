---
type: blog
date: 2022-01-22
title: Things I Learned by Reading 21 Books in 2021
author: Phil Busch
imagePath: /blog-images/2021books.jpg
isPublished: true
---
import ContainerizedImage from "../src/components/ContainerizedImage"
import books from "../src/images/blog-images/2021books.jpg"
import goodreads from "../src/images/blog-images/goodreads-screenshot.png"


<ContainerizedImage maxWidth={`400px`} className={`rounded`} src={books}/>

For a good portion of my career, I've enjoyed investing in my learning by reading books. What started as a membership in a developer book club at a previous job eventually turned into a yearly habit of reading two or three books about developing software.

Like most people on this planet, 2020 provided me with a lot of free time at home. Most years prior to the pandemic, I'd find myself traveling somewhere new. With travel not being much of an option in 2020, I started "traveling" mentally through books. I discovered the work of author [Peter Hessler](https://www.peterhessler.net/), who has written about his experiences living and traveling in Egypt and China, and I found that I could not put his books down. 

In late 2020, I became hooked on the idea that I could use books to travel everywhere, even if the pandemic was preventing me from traveling almost anywhere. I started using an app called [Goodreads](https://www.goodreads.com/) to discover new books, and to keep track of books that I was reading. 

As 2021 began, I found that my reading habits only grew. At the beginning of the year, I set a goal in Goodreads to read seven books in 2021. By the time the year was over, I had read *21* books! I've learned a few things about myself as I read throughout the year.

### I'm not reading just about software development anymore

When I started reading more, I wasn't thinking just about my career. I used to think about reading as something that I had to do in order to pick up new technical skills. I quickly learned in my first handful of books read in 2021 that reading could serve so many more purposes.

I loved being exposed to the stories that some of these authors had to tell. Learning about Billy Baker's journey of making friends in his adult life in _We Need to Hang Out: A Memoir of Making Friends_ brought a lot of relatable laughs in a time of staying at home. Kal Penn's autobiography, _You Can't Be Serious_, exposed me to the challenges he faced trying to break into Hollywood and avoid stereotypes. _Braver Than You Think_ brought me on a journey of an eye-opening trip around the world with Maggie Downs.

I was also exposed to meaty content with lots of detailed information to consume. _Working Effectively with Legacy Code_ by Michael Feathers is a book I should have read ten years ago. I was able to expand my technical skill set and reinforce many of my existing software development skills with that read. Reading _Capital_ by Thomas Piketty taught me that I have the ability to make it through a 650+ page academic economics book. 

### Reading books unrelated to my career made it easier to expand my career

While I was expanding the type of books that I was reading, I found myself expanding the topics of professional books that I read about as well. The professional books I read in 2021 were mostly not about delivering software, but rather they were about topics that were tangentially related to the industry and startups. Expanding my mind to read about so many people and places for enjoyment also allowed me to read about many different professional topics for enjoyment, too.

Last year, I started to take a broader interest in some of the business aspects of marketing and growing software companies. There has been a feedback loop between learning and application of my newly acquired knowledge in my professional life with this pivot. 

Last year, _Zero to One_ by Peter Thiel crashed Startup Thinking into my brain, and the ideas will not be leaving anytime soon. I can no longer look at my career without thinking about opportunities to be the last mover into a given market, different styles of pessimistic and optimistic thinking, and the key question in the book: "What important truth do very few people agree with you on?".

_80/20 Sales and Marketing_ by Perry Marshall got me thinking a lot about who my customers are in the work that I do, and how to effectively reach them using the Pareto principle. _Drive_ by Daniel Pink, [a book I wrote about on this blog](https://philbusch.com/blog/2021/09/drive-book-review), really got me noodling on what motivates me in my career. 

From a professional perspective, the books I read in 2021 opened the door to a whole world that I wasn't living in until very recently. The topics I discovered last year by reading are helping me grow new skills professionally at a rapid pace.

### I'm a sucker for gamification

The final thing I learned last year is that I am an absolute sucker for gamification tactics. Gamification, a recent trend in software to add video game style mechanics into non game software, is very present in Goodreads. Goodreads has features to track books read towards a yearly goal, as well as a progress bar that shows you percent completion of each book you're reading:

<ContainerizedImage maxWidth={`400px`} className={`rounded`} src={goodreads}/>


These techniques worked with me. I found myself enjoying seeing the number of books read increase, and the percent done metrics go up. I also found that having both the Goodreads app, as well as the [Milwaukee Public Library app](https://countycat.mcfls.org/screens/mobile.html) on my phone created a feedback loop that allowed me to go straight from book discovery in Goodreads to putting the book on hold at my local library. This allowed me to read more as I never learned about a book and then forgot to get it from the library and read it.

### 2022

In 2022, I plan to build on this habit that I formed last year. I've got more books I want to read this year than I could have ever imagined. I hope to build off what I learned in 2021 to learn much more this year. My reading goal for 2022 is *22* books.

Here's the full list of books I read in 2021:
- _Country Driving_ by Peter Hessler
- _Eric_ by Shaun Tan
- _Tightrope: Americans Reaching for Hope_ by Nicholas Kristof and Sheryl WuDunn
- _Here in Berlin_ by Crisitina Garcia
- _Zero to One_ by Peter Thiel
- _The Story of a Goat_ by Perumal Murugan
- _Fake Accounts_ by Lauren Oyler
- _We Need to Hang Out: A Memoir of Making Friends_ by Billy Baker
- _The Lost Continent_ by Bill Bryson
- _On the House_ by John Boehner
- _Giannis: The Improbable Rise of an NBA MVP_ by Mirin Fader
- _Drive_ by Daniel Pink
- _Working Effectively with Legacy Code_ by Michael Feathers
- _The Kite Runner_ by Khaled Hosseini
- _Behold the Dreamers_ by Imbolo Mbue
- _Capital in the Twenty-First Century_ by Thomas Piketty
- _80/20 Sales and Marketing_ by Perry Marshall
- _Behind the Beautiful Forevers: Life, Death, and Hope in a Mumbai Undercity_ by Katherine Boo
- _Braver Than You Think_ by Maggie Downs
- _You Can't Be Serious_ by Kal Penn
- _State of Terror_ by Hillary Rodham Clinton and Louise Penny 
