---
type: blog
date: 2021-08-31
title: A First Look at Hot Reload in Visual Studio 2022
author: Phil Busch
imagePath: /blog-images/vs-hot-reload-og.jpg
isPublished: true
---
import ContainerizedImage from "../src/components/ContainerizedImage"
import hotReloadApplyChanges from "../src/images/blog-images/hot-reload-apply-code-changes.png"
import hotReloadChangesAppliedSuccessfully from "../src/images/blog-images/hot-reload-changes-applied-successfully.png"
import hotReloadRemoteEditAndContinue from "../src/images/blog-images/hot-reload-remote-edit-and-continue.png"
import hotReloadErrorList from "../src/images/blog-images/hot-reload-error-list.png"

[One of the recent episodes of .NET Rocks](https://www.dotnetrocks.com/?show=1752) was very interesting. Carl Franklin and Richard Campbell interviewed Dimitry Layalin from Microsoft. He is a Program Manager working on multiple products, including Hot Reload, a new feature in .NET that allows for code to be changed at runtime while it is being debugged.

It was very interesting to hear him talk about the journey they have gone on at Microsoft about how they developed the feature, leveraging existing features of Visual Studio's Edit and Continue functionality, and how C++ development in Visual Studio lent itself well to the functionality. This episode has made me eager to check hot reload out for myself, so this week I downloaded Visual Studio 2022 Preview 3 to give it a try.

### The Happy Path

After I finished installing Visual Studio, I decided to spin up a new .NET 6 Console Application using the built in template. .NET 6 is also in preview, and I figured this was a good chance to try it out as well. Using the new, simplified console app template, I created an application that accepts user input infinitely. I started up the debugger by pressing the familiar play button in the ribbon, and set a breakpoint. 

Applying changes to the running code was a breeze. I changed a line of code in the application, and I went ahead and pressed a new button in the ribbon that allows for changes to be applied to the running application:

<ContainerizedImage maxWidth={`400px`} className={`rounded`} src={hotReloadApplyChanges}/>

Within a second, I saw the following message appear on the bottom of my screen:

<ContainerizedImage maxWidth={`400px`} className={`rounded`} src={hotReloadChangesAppliedSuccessfully}/>

### Finding the Limits

As I have sometimes found myself working in a legacy solutions, it is important to me to test out the limits of a new technology. I wanted to see how hot reload responded when it was used in a remote debugging scenario. I find that the remote debugger is a very valuable tool when the solution has many build errors that are due to knotty external dependencies, or when the running application is not entirely recreated from the code base in the Visual Studio solution. I was interested to see how the folks at Microsoft handled the hot reload functionality when interacting with a remote application.

I knew that this test was almost certainly going to end in some form of failure. Being able to change a remotely executing application from an IDE would be impossible magic. The running application is not guaranteed to be in a place where it can be modified, and even if it was, changing it would be a deployment of the newly compiled code to the server, which would not be desired in a development scenario. 

I built my new application in debug mode, and started the executable locally by finding it in the bin folder and double clicking on it. I clicked on Attach to Process under the Debug menu in Visual Studio as usual to attach to the running process. Debugging worked as it normally does. 

I made a code change, and went ahead and pressed the Apply Code Changes button on the ribbon. As expected, Visual Studio couldn't apply the changes. I was presented with the following dialog:

<ContainerizedImage maxWidth={`500px`} className={`rounded`} src={hotReloadRemoteEditAndContinue}/>

I decided to press edit, which put the following error in the Error List:

<ContainerizedImage className={`fluid`} src={hotReloadErrorList}/>

At this point, I'd need to rebuild my application to be able to remote debug any newly coded changes.

### Final thoughts

I think Hot Reload is a very valuable feature. For a correctly architected application that can be run locally, it is a game changer. Being able to nearly eliminate the feedback loop between writing a line of code and knowing whether or not it worked will allow for dramatic productivity gains for those stopping and restarting applications between changes today. 

*P.S.: If you're interested in the console app, you can find it on my GitHub page: https://github.com/philbo87/HotReloadTestApp* 