---
type: blog
date: 2022-02-20
title: Keeping my focus with Gitlab Workflow for Visual Studio Code
author: Phil Busch
imagePath: /blog-images/CoffeeVSCode.jpg
isPublished: true
---
import ContainerizedImage from "../src/components/ContainerizedImage"
import install from "../src/images/blog-images/gitlab-workflow-install.png"
import pat from "../src/images/blog-images/gitlab-workflow-set-pat.png"
import statusbar from "../src/images/blog-images/gitlab-workflow-status-bar.png"
import civalidatorcommand from "../src/images/blog-images/gitlab-workflow-ci-validator-command.png"
import civalidatoroutput from "../src/images/blog-images/gitlab-workflow-ci-validator.png"

Last week, I wrote a [post about Gitlab CI/CD](/blog/2022/02/gitlab-ci-dot-net-6-azure-function) and how I'm using it to deploy Azure Functions for a personal project. While writing that article, I discovered a fantastic extension for Visual Studio Code that I think is worth sharing. That extension is called the [Gitlab Workflow for Visual Studio Code](https://marketplace.visualstudio.com/items?itemName=GitLab.gitlab-workflow).

Visual Studio Code is often my IDE of choice on personal projects. Many of my personal projects use very lightweight technologies, such as Gatsby and Azure Functions, that are easy to develop without a full IDE. I don't want to spend a lot of money on something that I'm using to learn, so VS Code fits the bill. One of the benefits of using Visual Studio Code is that it has a rich extension ecosystem. This makes it easy to tailor my local development experience to the technologies I'm using. 

### Installation and Setup
Installing the extension was very easy. I found the extension using the Extensions Marketplace in VS Code:
<ContainerizedImage maxWidth={`700px`} className={`rounded`} src={install}/>

I then created a Personal Access Token in Gitlab, which [I could do on this page](https://gitlab.com/-/profile/personal_access_tokens). I had to configure the extension to use the newly created PAT. That was easy - there is a command built included with extension to do that. I could find it in the Command Palette:
<ContainerizedImage maxWidth={`700px`} className={`rounded`} src={pat}/>

### Using the Extension
I can use the extension on any Gitlab repository open in Visual Studio Code. The extension is smart enough to know when its features are available. I've explored two of the features so far.

#### Pipeline Status
My favorite feature of the extension is its integration with Gitlab CI/CD Pipelines. My projects initiate a CI pipeline on every commit to the main branch. Prior to using this extension, I would manually go to Gitlab in a web browser to check the status and make sure my pipeline was passing. With the Gitlab Workflow extension, I can now see a pipeline's execution progress and status in the VS Code Status bar. I value this feature as it allows me to keep writing code and not break focus. It is also great when I am working with one screen.

<ContainerizedImage maxWidth={`500px`} className={`rounded`} src={statusbar}/>

#### Validating `.gitlab-ci.yml`
Something that is always tricky with YAML-based DevOps pipelines is ensuring the syntax in the YAML file is correct. From incorrect usage of tabs, to the wrong syntax, there is some learning curve to this powerful paradigm. I have spent many hours of my life checking in YAML files for Gitlab and other DevOps tools only to find out that I did not write that file correctly when the pipeline executes. This feedback loop is slow and cumbersome.

The Gitlab Workflow Extension has a useful feature that allows `.gitlab-ci.yml` to be validated without pushing changes to the remote repository. Using it is simple. With `.gitlab-ci.yml` open in my active editor, there is a command available in the Command Palette:

<ContainerizedImage maxWidth={`500px`} className={`rounded`} src={civalidatorcommand}/>

This command parses the file, and will provide toast messages of any problems found:
<ContainerizedImage maxWidth={`500px`} className={`rounded`} src={civalidatoroutput}/>

### Just scratching the surface
I'm just beginning to scratch the surface on the Gitlab Workflow Extension. As I learn more, I'll share my findings. I hope that this post introduced you to something new!

If you're interested in learning more, you can find the docs for the extension [here](https://docs.gitlab.com/ee/user/project/repository/vscode.html). You can also find the extension's source code [here](https://gitlab.com/gitlab-org/gitlab-vscode-extension#extension-settings).
