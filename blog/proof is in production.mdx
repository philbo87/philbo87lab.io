---
type: blog
date: 2021-08-18
title: The Proof is in Production
author: Phil Busch
imagePath: /blog-images/two-people-working-coding.jpg
isPublished: true
---
How do you know that the work you do has created value? 

You have a customer for the software you deliver. Ideally, someone is attempting to use the solution you work on.

You may have product owners, product managers, project managers, business analysts, sales teams, and more between you and the customer using your product. Or, maybe you are consulting and you work directly with your client. 

No matter what the working arrangements look like between you and the end users you serve, there is a good chance that all folks involved want to be sure that a great solution is being delivered. There is almost certainly great care between all parties involved to make sure things are just right, that they are done correctly, and that the customer is being served well.

This amount of care can naturally create disagreements and tension. Chances are, if you've been delivering software for more than a few months, you've started to hear questions like these:

- What is the right thing to build?
- How should page look?
- When is it going to be done?
- Do we need to architect this solution differently?

There is a simple solution to these disagreements: **Deploy to Production. Deploy early. Deploy often. Deploy as much as possible.**

Deploying code to production solves nearly all disagreements. By surfacing the solution to users, customers can get value immediately. The sooner users click the buttons on your web app, the sooner you and your peers at your company can find out if people can use the solution to solve their problems. Or, the sooner you find out that choosing hot pink was not a great color choice for that button! 😀 

Of course, none of this matters if you don't apply the learnings gained from the deploy to production to a future effort. You have to be intentional about using the knowledge gained. You may learn that the customers love the solution and that you don't need to add more to it - and that is great! Or, you may learn that it is time to go back to the drawing board and start over. That's OK too. Likely, it will be somewhere in between. No matter what you learn, the key is to make the decision actively about what to work on next based on that feedback, and not passively based on a long list of old backlog items. 

If you don't deploy to production often, ask yourself why that is. Is it something you can change with a conversation with your team? Is it something that was decided outside your team? Can you talk to that person? 

There may be lots of reasons that you don't deploy frequently - fear of change, infrastructure limitations, cross-company communication issues are just a few that I have experienced. Don't let that stop you - keep looking for the opportunity to try frequent, iterative production deployments. Products with lower stakes, or off hours deployments may be good places to start in your organization. 

Do not let one roadblock keep you from finding a path to an iterative approach. Ultimately, deploying frequently on one application is better than infrequent deployments. And if you can deploy frequently on one application, eventually the learning gained from that will be contagious in your organization and solve the disagreements on going to production often.